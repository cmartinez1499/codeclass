# CodeClass

Welcome To Class

# Mac instrcutions
install https://www.iterm2.com/

install homebrew by following the instructions - https://brew.sh/

install zsh with this terminal command - 
```shell
brew install zsh
```

make zsh default by using this command - 
```shell
sudo -s 'echo /usr/local/bin/zsh >> /etc/shells' && chsh -s /usr/local/bin/zsh
```
install oh my zsh with this command - 
```shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

# Assignment 3
Start coding:

This assignment you need to set up your envoirnment to run a web application. 

First you need to install Node. 

On Mac
```shell
brew install node
```

On Linux (brandon i might have to help you with this)
```shell
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Something that could also help is installing some plugins into Sublime

I can help you guys out in some of these

One good thing to do is install a theme 

here are some examples https://dev.to/kazup/10-best-sublime-text-themes-in-2018-53j5

I personally do https://github.com/kenwheeler/brogrammer-theme

Once you have installed node. I will help you set up the struction for a React.js application.


